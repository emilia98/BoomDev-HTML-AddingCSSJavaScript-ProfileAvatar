import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready
  const button = document.querySelector(".button");
  button.addEventListener("click", () => {
    alert("💣");
  });

  const image = document.querySelector(".image");
  image.addEventListener("click", () => {
    image.style.transform = "scale(2)";
  });
  
  /*
  image.addEventListener("click", () => {
    let isAlreadySelected = image.className.includes("selected");
    image.className = `image ${isAlreadySelected ? "" : "selected"}`;
    image.style.transform = `scale(${isAlreadySelected ? 1 : 2})`;
  }); */

  /*
  let scaleFactor = 1;
  image.addEventListener("click", () => {
    scaleFactor *= 2;
    image.style.transform = `scale(${scaleFactor})`;
  }); */
});